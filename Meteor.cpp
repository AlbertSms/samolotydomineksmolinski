#include "Meteor.h"

void Meteor::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    for(int i=0; i<meteor.size(); i++)
        target.draw(meteor[i], states);
}

void Meteor::update(float delta, sf::Vector2f wektor)
{
    for(int i=0; i<meteor.size(); i++)
    {
        meteor[i].move(wektor.x*delta, wektor.y*delta);
        rect[i] = meteor[i].getGlobalBounds();
    }

}
sf::FloatRect Meteor::getRect(int index) const
{
    return rect[index];
}

sf::Sprite Meteor::getSprite(int index) const
{
    return meteor[index];
}

int Meteor::getAmount() const
{
    return meteor.size();
}

void Meteor::add(int amount)
{
    for(int i=0; i<amount; i++)
    {
        int index = rand()%20;

        sf::Sprite sprite;
        sf::FloatRect rectangle;

        sprite.setTexture(texture[index]);

        rectangle = sprite.getGlobalBounds();
        sprite.setOrigin(rectangle.width/2, rectangle.height/2);

        sprite.setPosition(rand()%3000, rand()%3000);

        rect.push_back(rectangle);
        meteor.push_back(sprite);

    }
}

void Meteor::remover(int index)
{
    for(int i=index; i<meteor.size()-1; i++)
    {
        std::swap(meteor[i], meteor[i+1]);
        std::swap(rect[i], rect[i+1]);
    }
    meteor.resize(meteor.size()-1);
    rect.resize(meteor.size());
}

Meteor::Meteor()
{
    srand(time(NULL));

    texture[0].loadFromFile("data/textures/meteoryd1.png");
    texture[1].loadFromFile("data/textures/meteoryd2.png");
    texture[2].loadFromFile("data/textures/meteoryd3.png");
    texture[3].loadFromFile("data/textures/meteoryd4.png");
    texture[4].loadFromFile("data/textures/meteoryd5.png");
    texture[5].loadFromFile("data/textures/meteoryd6.png");
    texture[6].loadFromFile("data/textures/meteoryd7.png");
    texture[7].loadFromFile("data/textures/meteoryd8.png");
    texture[8].loadFromFile("data/textures/meteoryd9.png");
    texture[9].loadFromFile("data/textures/meteoryd10.png");
    texture[10].loadFromFile("data/textures/meteoryd11.png");
    texture[11].loadFromFile("data/textures/meteoryd12.png");
    texture[12].loadFromFile("data/textures/meteoryd13.png");
    texture[13].loadFromFile("data/textures/meteoryd14.png");
    texture[14].loadFromFile("data/textures/meteoryd15.png");

}
